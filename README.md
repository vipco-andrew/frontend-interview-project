# Introduction 

This is the technical interview project for Viewpoint Investment Partner's Front-End Developer position. For the interview, please have an environment ready that can clone, install dependencies, run, and debug this Angular project. During the interview, a small number of tasks will be given to you to complete.

# Getting Started

Clone this repo:
```
git clone https://bitbucket.org/vipco-andrew/frontend-interview-project/src/master/
```

Install the npm dependencies:
```
npm install
```

Run the project:
```
npm start
```