import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SecurityDataComponent } from './components/security-data/security-data.component';
import { MusicSearchComponent } from './components/music-search/music-search.component';

const routes: Routes = [
  { path: 'music-search', component: MusicSearchComponent },
  { path: 'security-data', component: SecurityDataComponent },
  { path: '**', redirectTo: 'security-data', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
