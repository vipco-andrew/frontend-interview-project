import { Component, Input, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
import { ISecurityData } from '../interfaces/security-data.interface';

import { ITimeStampData } from '../interfaces/timestamp-data.interface';

declare var require: any;
let Boost = require('highcharts/modules/boost');
let noData = require('highcharts/modules/no-data-to-display');
let More = require('highcharts/highcharts-more');

Boost(Highcharts);
noData(Highcharts);
More(Highcharts);
noData(Highcharts);

@Component({
  selector: 'app-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.css']
})
export class GraphComponent implements OnInit {

  @Input() security!: ISecurityData;

  Highcharts: typeof Highcharts = Highcharts;
  chartOptions!: Highcharts.Options;

  constructor() { }

  ngOnInit(): void {
    this.chartOptions = this.setGraph(this.security);
  }

  private setGraph(data: ISecurityData): Highcharts.Options {
    return {
      chart: {
        zoomType: 'x',
        backgroundColor: '#fff',
      },
      credits: {
        enabled: false
      },
      title: {
        text: data.info.ticker
      },
      plotOptions: {
        line: {
          color: '#508f7d'
        }
      },
      tooltip: {
        valueDecimals: 2
      },
  
      xAxis: {
        type: 'datetime'
      },
      yAxis: {
        title: {
          text: 'Value'
        }
      },
      series: [
        {
          data: data.data.map(entry => [+new Date(entry.date), entry.field]),
          name: data.info.field,
          type: 'line',
        }
      ]
    };
  }
}
