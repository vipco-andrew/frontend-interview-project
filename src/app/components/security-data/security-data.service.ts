import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { ISecurityInfo } from './interfaces/security-info.interface';
import { ITimeStampData } from './interfaces/timestamp-data.interface';
import { ICountry } from './interfaces/country.interface';

@Injectable({
  providedIn: 'root'
})
export class SecurityDataService {

  private securitiesInfoURL = `https://vipco-onboarding-api.azurewebsites.net/securities`;
  private securityDataURL = `https://vipco-onboarding-api.azurewebsites.net/securitydata`;

  private countryLookup: { [key: string]: string } = {
    'ca': 'Canada',
    'au': 'Australia',
    'de': 'Germany',
    'fr': 'France',
    'jp': 'Japan',
    'gb': 'United Kingdom',
    'us': 'United States',
    'ch': 'Switzerland',
  }

  constructor(private http: HttpClient) { }

  getCountries(): Observable<ICountry[]> {
    return this.http.get<ISecurityInfo[]>(this.securitiesInfoURL)
      .pipe(
        map(data => {
          const countries = data.map(x => x.country);
          const distinct = [...new Set(countries)];
          return distinct.map(x => ({
            'name': this.countryLookup[x],
            'code': x
          }))
        })
      )
  }

  getSecuritiesForCountry(country: string): Observable<ISecurityInfo[]> {
    return this.http.get<any[]>(this.securitiesInfoURL)
      .pipe(
        map(tickers => {
          return tickers.filter((entry) => entry.country === country)
        }),
      );
  }

  getSecurityData(ticker: string, startDate: Date, endDate: Date): Observable<ITimeStampData[]> {
    
    let url = `${this.securityDataURL}/${ticker}`;

    const params: any = { ticker: ticker };
    if (startDate) {
      params['start'] = moment(startDate).format('YYYY-MM-DD');
    }
    if (endDate) {
      params['end'] = moment(endDate).format('YYYY-MM-DD');
    }

    return this.http.get<any[]>(url, { params });
  }
}
