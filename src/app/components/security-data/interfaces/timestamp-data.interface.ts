
export interface ITimeStampData {
  date: string;
  field: number;
}
