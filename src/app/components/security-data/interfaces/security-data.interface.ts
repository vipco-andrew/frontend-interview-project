import { ISecurityInfo } from "./security-info.interface";
import { ITimeStampData } from "./timestamp-data.interface";

export interface ISecurityData {
    info: ISecurityInfo;
    data: ITimeStampData[]
}
