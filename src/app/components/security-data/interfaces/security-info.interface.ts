
export interface ISecurityInfo {
    ticker: string;
    type: string;
    field: string;
    country: string;
}
