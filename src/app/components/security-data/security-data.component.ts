import { FormBuilder, FormGroup } from '@angular/forms'
import { Component, ChangeDetectionStrategy } from '@angular/core';
import { Observable, forkJoin, EMPTY, BehaviorSubject, } from 'rxjs';
import { map, tap, switchMap, startWith, finalize } from 'rxjs/operators';

import { ICountry } from './interfaces/country.interface';
import { ISecurityInfo } from './interfaces/security-info.interface';
import { SecurityDataService } from './security-data.service';
import { ISecurityData } from './interfaces/security-data.interface';

@Component({
  selector: 'security-data',
  templateUrl: './security-data.component.html',
  styleUrls: ['./security-data.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SecurityDataComponent {

  countries$: Observable<ICountry[]>;
  securities$: Observable<ISecurityInfo[]>;

  securityData$ = new BehaviorSubject<ISecurityData[]>([]);
  
  loading: boolean = false;

  securitiesForm: FormGroup = this.fb.group({
    country: ['ca'],
    securities: [[]],
    startDate: [null],
    endDate: [null]
  }, { validator: this.checkDates });

  constructor(private securityDataService: SecurityDataService, private fb: FormBuilder) {

    // Countries
    this.countries$ = this.securityDataService.getCountries();

    // Securities for selected country
    this.securities$ = this.securitiesForm.get('country')?.valueChanges
      .pipe(
        startWith('ca'),
        switchMap((countryCode: any) => this.securityDataService.getSecuritiesForCountry(countryCode)),
        tap(data => {
          this.resetDatePickersChart();
          this.securitiesForm.patchValue({
            securities: data
          })
        })
      ) ?? EMPTY;

    // Data for selected securities
    this.securitiesForm.valueChanges
      .subscribe(formInput => {
        this.getSecurityData(formInput.startDate, formInput.endDate, formInput.securities)
      });
  }

  private getSecurityData(start: Date, end: Date, securities: ISecurityInfo[]) {
    this.loading = true;
    const obs: Observable<ISecurityData>[] = securities.map((security: ISecurityInfo) => {
      return this.securityDataService.getSecurityData(security.ticker, start, end)
        .pipe(
          map(resultFromApi => ({
            data: resultFromApi,
            info: security
          })),
          finalize(() => this.loading = false)
        )
    })
    forkJoin(obs).subscribe(result => this.securityData$.next(result));
  }

  private resetDatePickersChart(): void {
    this.securitiesForm.patchValue({
      startDate: null,
      endDate: null
    })
  }

  private checkDates(group: FormGroup) {
    if (group.controls.endDate.value) {
      if (group.controls.endDate.value < group.controls.startDate.value) {
        return { invalidRange: true }
      }
    }
    return null;
  }
}
