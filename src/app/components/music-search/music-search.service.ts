import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IMusicData } from './interfaces/music-data.interface';

@Injectable({
  providedIn: 'root'
})
export class MusicSearchService {

  constructor(private http: HttpClient) { }

  getResults(artistSearch: any[]): Observable<IMusicData[]> {
    return this.http.get<IMusicData[]>(`https://itunes.apple.com/search?term=${artistSearch}&media=music&limit=20`)
  }
}
