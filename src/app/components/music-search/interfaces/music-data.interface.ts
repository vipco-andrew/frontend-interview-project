export interface IMusicData {
  trackName: string;
  artistName: string;
  trackViewUrl: string;
  artworkUrl100: string;
  primaryGenreName: string;
}
