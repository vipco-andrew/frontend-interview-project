import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { IMusicData } from './interfaces/music-data.interface';
import { TEMP_DATA } from './music-search.data';


@Component({
  selector: 'music-search',
  templateUrl: './music-search.component.html',
  styleUrls: ['./music-search.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MusicSearchComponent implements OnInit {

  loading: boolean = false;
  searchResults$?: Observable<IMusicData[]> = undefined;
  searchInput = new FormControl('');

  constructor() { }

  ngOnInit(): void {
    this.searchResults$ = of(TEMP_DATA)
  }
}
